import pytest
import dog_sender


def test_get_random_dog_file():
    dog = dog_sender.DogSender()
    dog._get_random_dog_file()
    assert type(dog._url) == str


def test_parse_file_type():
    dog = dog_sender.DogSender()
    dog._get_random_dog_file()
    dog._parse_file_type()
    assert dog._type in ['jpg', 'png', 'jpeg', 'gif', 'mp4', 'webm']

