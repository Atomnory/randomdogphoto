import os
import logging
from telegram.ext import Updater, CommandHandler
import handlers


API_TOKEN = os.getenv('TELEGRAM_API_TOKEN_DogPhoto')
updater = Updater(token=API_TOKEN)

dispatcher = updater.dispatcher

logging.basicConfig(level=logging.INFO)

start_handler = CommandHandler('start', handlers.start)
dispatcher.add_handler(start_handler)

more_handler = CommandHandler('more', handlers.more)
dispatcher.add_handler(more_handler)

updater.start_polling()
