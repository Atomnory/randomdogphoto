import requests


class DogSender:
    def __init__(self):
        self._request_url = 'https://random.dog/woof.json'
        self._url = None
        self._type = None

    def _get_random_dog_file(self):
        response = requests.get(self._request_url)
        self._url = response.json()['url']

    def _parse_file_type(self):
        url_parts = self._url.split('.')
        self._type = url_parts[2].lower()

    def send_dog_file(self, update, context):
        self._get_random_dog_file()
        self._parse_file_type()

        if self._type in ['jpg', 'png', 'jpeg']:
            context.bot.send_photo(chat_id=update.effective_chat.id, photo=self._url)
        elif self._type in ['gif']:
            context.bot.send_animation(chat_id=update.effective_chat.id, animation=self._url)
        elif self._type in ['mp4', 'webm']:
            context.bot.send_video(chat_id=update.effective_chat.id, video=self._url)
        else:
            raise Exception('unknown file type: ', self._type)

